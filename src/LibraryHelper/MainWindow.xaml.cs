﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LibraryHelper.Dialogs;
using LibraryHelper.Objects;
using LibraryHelper.Singleton;

namespace LibraryHelper
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private uint LastSelectedList
        { get; set; }

        public MainWindow()
        {
            InitializeComponent();

            var author1 = new Author()
            {
                Books = new List<Book>(),
                Genres = new List<Genre>()
            };
            var author2 = new Author()
            {
                Books = new List<Book>(),
                Genres = new List<Genre>()
            };
            var author3 = new Author()
            {
                Books = new List<Book>(),
                Genres = new List<Genre>()
            };
            var author4 = new Author()
            {
                Books = new List<Book>(),
                Genres = new List<Genre>()
            };
            var author5 = new Author()
            {
                Books = new List<Book>(),
                Genres = new List<Genre>()
            };

            var book1 = new Book(); 
            var book2 = new Book();
            var book3 = new Book();
            var book4 = new Book();
            var book5 = new Book();
            var book6 = new Book();
            var book7 = new Book();

            var user1 = new User()
            {
                Books = new List<Book>()
            };
            var user2 = new User()
            {
                Books = new List<Book>()
            };
            var user3 = new User()
            {
                Books = new List<Book>()
            };

            book1.Author = author1; author1.Books.Add(book1);
            book2.Author = author2; author2.Books.Add(book2);
            book3.Author = author3; author3.Books.Add(book3);
            book4.Author = author4; author4.Books.Add(book4);
            book5.Author = author1; author1.Books.Add(book5);
            book6.Author = author2; author2.Books.Add(book6);
            book7.Author = author2; author2.Books.Add(book7);

            book1.InStock = 3;
            book2.InStock = 1;
            book3.InStock = 6;
            book4.InStock = 5;
            book5.InStock = 11;
            book6.InStock = 7;
            book7.InStock = 6;

            book1.ID = 0;
            book2.ID = 1;
            book3.ID = 2;
            book4.ID = 3;
            book5.ID = 4;
            book6.ID = 5;
            book7.ID = 6;

            book1.Genre = Genre.SciFi;
            book2.Genre = Genre.Fantasy;
            book3.Genre = Genre.Dictionary;
            book4.Genre = Genre.Crime;
            book5.Genre = Genre.SciFi;
            book6.Genre = Genre.Manual;
            book7.Genre = Genre.SciFi;

            book1.Year = 1994;
            book2.Year = 1965;
            book3.Year = 1981;
            book4.Year = 2001;
            book5.Year = 1979;
            book6.Year = 2005;
            book7.Year = 1992;

            book1.Title = "Once Upon a Time";
            book2.Title = "Adventures";
            book3.Title = "Dead Man Tales";
            book4.Title = "The B";
            book5.Title = "Nothing Free";
            book6.Title = "Story of Rome";
            book7.Title = "Kepler";

            user1.Name = "Piotr";
            user1.Surname = "Nowak";
            user1.ContactData = new ContactData()
            {
                Phone = "666555333",
                Email = "piotr.nowak@wps.com",
                Address = new Address()
                {
                    Street = "Orzeszkowa",
                    Number = "15\\13",
                    PostalCode = "32-221",
                    City = "Kraków"
                }
            };

            user1.Books.Add(book3);
            user1.Books.Add(book2);
            user1.Books.Add(book6);

            user2.Name = "Agnieszka";
            user2.Surname = "Kotz";
            user2.ContactData = new ContactData()
            {
                Phone = "888000333",
                Email = "agnieszka.kotz@wps.com",
                Address = new Address()
                {
                    Street = "Wesoła",
                    Number = "123",
                    PostalCode = "79-009",
                    City = "Gdańsk"
                }
            };

            user2.Books.Add(book1);
            user2.Books.Add(book5);

            user3.Name = "Iza";
            user3.Surname = "Nowak";
            user3.ContactData = new ContactData()
            {
                Phone = "555666111",
                Email = "iza.nowak@wps.com",
                Address = new Address()
                {
                    Street = "Sezamkowa",
                    Number = "3A",
                    PostalCode = "56-888",
                    City = "Wrocław"
                }
            };

            author1.Name = "John";
            author2.Name = "Jakar";
            author3.Name = "Jakub";
            author4.Name = "Wendy";
            author5.Name = "Joshua";

            author1.Surname = "Newman";
            author2.Surname = "Haduba";
            author3.Surname = "Stańko";
            author4.Surname = "Wez";
            author5.Surname = "Craten";

            author1.Genres.Add(Genre.Manual);
            author1.Genres.Add(Genre.SciFi);
            author2.Genres.Add(Genre.Fantasy);
            author2.Genres.Add(Genre.Crime);
            author3.Genres.Add(Genre.Drama);
            author4.Genres.Add(Genre.Romance);
            author5.Genres.Add(Genre.Drama);

            AuthorCollection.Instance.Collection.Add(author1);
            AuthorCollection.Instance.Collection.Add(author2);
            AuthorCollection.Instance.Collection.Add(author3);
            AuthorCollection.Instance.Collection.Add(author4);
            AuthorCollection.Instance.Collection.Add(author5);

            BookCollection.Instance.Collection.Add(book1);
            BookCollection.Instance.Collection.Add(book2);
            BookCollection.Instance.Collection.Add(book3);
            BookCollection.Instance.Collection.Add(book4);
            BookCollection.Instance.Collection.Add(book5);
            BookCollection.Instance.Collection.Add(book6);
            BookCollection.Instance.Collection.Add(book7);

            UserCollection.Instance.Collection.Add(user1);
            UserCollection.Instance.Collection.Add(user2);
            UserCollection.Instance.Collection.Add(user3);

            this.BookPicker.Items.Add(book1);
            this.BookPicker.Items.Add(book2);
            this.BookPicker.Items.Add(book3);
            this.BookPicker.Items.Add(book4);
            this.BookPicker.Items.Add(book5);
            this.BookPicker.Items.Add(book6);
            this.BookPicker.Items.Add(book7);

            this.UserPicker.Items.Add(user1);
            this.UserPicker.Items.Add(user2);
            this.UserPicker.Items.Add(user3);

            this.AuthorPicker.Items.Add(author1);
            this.AuthorPicker.Items.Add(author2);
            this.AuthorPicker.Items.Add(author3);
            this.AuthorPicker.Items.Add(author4);
            this.AuthorPicker.Items.Add(author5);
        }

        private void AddUser_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new AddUserWindow();
            dialog.ShowDialog();   
        }

        private void AddAuthor_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new AddAuthorWindow();
            dialog.ShowDialog();   
        }

        private void AddBook_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new AddBookWindow();
            dialog.ShowDialog();   
        }

        private void ShowDetails_Click(object sender, RoutedEventArgs e)
        {
            if(LastSelectedList == 0)
            {
                var dialog = new UserDetailsWindow((User)this.UserPicker.SelectedItem);
                dialog.ShowDialog();
            }
            else if(LastSelectedList == 1)
            {
                var dialog = new AuthorDetailsWindow((Author)this.AuthorPicker.SelectedItem);
                dialog.ShowDialog();
            }
            else
            {
                var dialog = new BookDetailsWindow((Book)this.BookPicker.SelectedItem);
                dialog.ShowDialog();
            }
        }

        private void GiveOutBook_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new GiveOutBookWindow();
            dialog.ShowDialog();
        }

        private void ReturnBook_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new ReturnBookWindow();
            dialog.ShowDialog();
        }

        private void UserPicker_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            LastSelectedList = 0;
        }

        private void AuthorPicker_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            LastSelectedList = 1;
        }

        private void BookPicker_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            LastSelectedList = 2;
        }
    }
}
