﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using LibraryHelper.Objects;
namespace LibraryHelper.Dialogs
{
    /// <summary>
    /// Interaction logic for BookDetailsWindow.xaml
    /// </summary>
    public partial class BookDetailsWindow : Window
    {
        public BookDetailsWindow(Book book)
        {
            InitializeComponent();
            Title.Content = book.Title.ToString();
            Year.Content = book.Year.ToString();
            ID.Content = book.ID.ToString();
            Genre.Content = book.Genre.ToString();
            InStock.Content = book.InStock.ToString();
            Author.Content = book.Author.Name.ToString() + " " + book.Author.Surname.ToString();
        }
    }
}
