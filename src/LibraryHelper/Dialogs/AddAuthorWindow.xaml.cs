﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using LibraryHelper.Singleton;
using LibraryHelper.Objects;

namespace LibraryHelper.Dialogs
{
    /// <summary>
    /// Interaction logic for AddAuthorWindow.xaml
    /// </summary>
    public partial class AddAuthorWindow : Window
    {
        public AddAuthorWindow()
        {
            InitializeComponent();
            GenresToPick.Items.Add(LibraryHelper.Genre.Crime);
            GenresToPick.Items.Add(LibraryHelper.Genre.Dictionary);
            GenresToPick.Items.Add(LibraryHelper.Genre.Drama);
            GenresToPick.Items.Add(LibraryHelper.Genre.Fantasy);
            GenresToPick.Items.Add(LibraryHelper.Genre.Horror);
            GenresToPick.Items.Add(LibraryHelper.Genre.Manual);
            GenresToPick.Items.Add(LibraryHelper.Genre.Romance);
            GenresToPick.Items.Add(LibraryHelper.Genre.SciFi);
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            Genres.Items.Add(GenresToPick.SelectedItem);
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            var author = new Author()
                {
                    Name = this.Name.Text,
                    Surname = this.Surname.Text,
                    Books = new List<Book>(),
                    Genres = new List<Genre>()
                };

            foreach(var item in Genres.Items)
            {
                author.Genres.Add((Genre)item);
            }

            AuthorCollection.Instance.Collection.Add(author);
            this.Close();
        }
    }
}
