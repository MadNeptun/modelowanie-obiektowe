﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using LibraryHelper.Singleton;
using LibraryHelper.Objects;
namespace LibraryHelper.Dialogs
{
    /// <summary>
    /// Interaction logic for ReturnBookWindow.xaml
    /// </summary>
    public partial class ReturnBookWindow : Window
    {
        public ReturnBookWindow()
        {
            InitializeComponent();
            foreach(User u in UserCollection.Instance.Collection)
            {
                User.Items.Add(u);
            }
        }

        private void User_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            foreach(Book b in ((User)User.SelectedItem).Books)
            {
                Book.Items.Add(b);
            }
        }

        private void Return_Click(object sender, RoutedEventArgs e)
        {
            ((Book)Book.SelectedItem).Return();
            ((User)User.SelectedItem).Books.Remove(((Book)Book.SelectedItem));
            this.Close();
        }
    }
}
