﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using LibraryHelper.Objects;
namespace LibraryHelper.Dialogs
{
    /// <summary>
    /// Interaction logic for UserDetailWindow.xaml
    /// </summary>
    public partial class UserDetailsWindow : Window
    {
        public UserDetailsWindow(User user)
        {
            InitializeComponent();
            Name.Content = user.Name + " " + user.Surname;
            Street.Content = user.ContactData.Address.Street + " " + user.ContactData.Address.Number;
            City.Content = user.ContactData.Address.PostalCode + " " + user.ContactData.Address.City;
            Phone.Content = user.ContactData.Phone.ToString();
            Email.Content = user.ContactData.Email.ToString();
            foreach(Book b in user.Books)
            {
                Books.Items.Add(b);
            }
        }
    }
}
