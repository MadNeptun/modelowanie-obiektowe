﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using LibraryHelper.Singleton;
using LibraryHelper.Objects;
namespace LibraryHelper.Dialogs
{
    /// <summary>
    /// Interaction logic for AddUserWindow.xaml
    /// </summary>
    public partial class AddUserWindow : Window
    {
        public AddUserWindow()
        {
            InitializeComponent();
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            UserCollection.Instance.Collection.Add(new User()
            {
                Books = new List<Book>(),
                Name = this.Name.Text,
                Surname = this.Surname.Text,
                ContactData = new ContactData()
                {
                    Phone = this.Phone.Text,
                    Email = this.Email.Text,
                    Address = new Address()
                    {
                        Street = this.Street.Text,
                        Number = this.Number.Text,
                        PostalCode = this.PostalCode.Text,
                        City = this.City.Text
                    }
                }
            });
            this.Close();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
