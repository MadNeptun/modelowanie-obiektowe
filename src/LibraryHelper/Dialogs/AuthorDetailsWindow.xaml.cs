﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using LibraryHelper.Objects;

namespace LibraryHelper.Dialogs
{
    /// <summary>
    /// Interaction logic for AuthorDetailsWindow.xaml
    /// </summary>
    public partial class AuthorDetailsWindow : Window
    {
        public AuthorDetailsWindow(Author author)
        {
            InitializeComponent();
            Name.Content = author.Name + " " + author.Surname;
            foreach(Genre g in author.Genres)
            {
                Genres.Items.Add(g);
            }
            foreach(Book b in author.Books)
            {
                Books.Items.Add(b);
            }
        }
    }
}
