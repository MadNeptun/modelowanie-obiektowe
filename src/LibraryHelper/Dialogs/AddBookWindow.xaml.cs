﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using LibraryHelper.Objects;
using LibraryHelper.Singleton;

namespace LibraryHelper.Dialogs
{
    /// <summary>
    /// Interaction logic for AddBookWindow.xaml
    /// </summary>
    public partial class AddBookWindow : Window
    {
        public AddBookWindow()
        {
            InitializeComponent();
            Genre.Items.Add(LibraryHelper.Genre.Crime);
            Genre.Items.Add(LibraryHelper.Genre.Dictionary);
            Genre.Items.Add(LibraryHelper.Genre.Drama);
            Genre.Items.Add(LibraryHelper.Genre.Fantasy);
            Genre.Items.Add(LibraryHelper.Genre.Horror);
            Genre.Items.Add(LibraryHelper.Genre.Manual);
            Genre.Items.Add(LibraryHelper.Genre.Romance);
            Genre.Items.Add(LibraryHelper.Genre.SciFi);
            foreach(Author a in AuthorCollection.Instance.Collection)
                Author.Items.Add(a);
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            var book = new Book()
                {
                    Title = this.Title.Text,
                    Year = uint.Parse(this.Year.Text),
                    Genre = (Genre)this.Genre.SelectedItem,
                    Author = (Author)this.Author.SelectedItem,
                    InStock = uint.Parse(this.Amount.Text),
                    ID = (uint)(BookCollection.Instance.Collection.Count + 1)
                };
            ((Author)this.Author.SelectedItem).Books.Add(book);
            BookCollection.Instance.Collection.Add(book);
            this.Close();
        }
    }
}
