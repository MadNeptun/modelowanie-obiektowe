﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using LibraryHelper.Objects;
using LibraryHelper.Singleton;

namespace LibraryHelper.Dialogs
{
    /// <summary>
    /// Interaction logic for GiveOutBookWindow.xaml
    /// </summary>
    public partial class GiveOutBookWindow : Window
    {
        public GiveOutBookWindow()
        {
            InitializeComponent();
            foreach (User u in UserCollection.Instance.Collection)
            {
                User.Items.Add(u);
            }
        }

        private void User_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            foreach (Book b in BookCollection.Instance.Collection)
            {
                if(b.InStock>0)
                {
                    Book.Items.Add(b);
                }
            }
        }

        private void GiveOut_Click(object sender, RoutedEventArgs e)
        {
            ((Book)Book.SelectedItem).GiveOut();
            ((User)User.SelectedItem).Books.Add(((Book)Book.SelectedItem));
            this.Close();
        }
    }
}
