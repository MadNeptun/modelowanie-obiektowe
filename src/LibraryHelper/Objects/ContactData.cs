﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryHelper.Objects
{
    public class ContactData
    {
        public Address Address
        { get; set; }

        public string Phone
        { get; set; }

        public string Email
        { get; set; }
    }
}
