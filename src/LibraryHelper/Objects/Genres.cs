﻿namespace LibraryHelper
{
    public enum Genre
    {
        Drama,
        Crime,
        SciFi,
        Horror,
        Dictionary,
        Manual,
        Romance,
        Fantasy
    }
}