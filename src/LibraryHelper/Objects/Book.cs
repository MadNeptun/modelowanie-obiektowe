﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryHelper.Objects
{
    public class Book
    {
        public Author Author
        { get; set; }

        public string Title
        { get; set; }

        public uint Year
        { get; set; }

        public uint ID
        { get; set; }

        public Genre Genre
        { get; set; }

        public uint InStock
        { get; set; }

        public void GiveOut()
        {
            InStock--;
        }

        public void Return()
        {
            InStock++;
        }

        public override string ToString()
        {
            return Title + " (" + Year + ")";
        }

    }
}
