﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryHelper.Objects
{
    public class Author
    {
        public string Name
        { get; set; }

        public string Surname
        { get; set; }

        public List<Book> Books
        { get; set; }

        public List<Genre> Genres
        { get; set; }

        public override string ToString()
        {
            return Name + " " + Surname;
        }
    }
}
