﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraryHelper.Objects;

namespace LibraryHelper.Singleton
{
    public class BookCollection
    {
        private static BookCollection instance = null;

        public static BookCollection Instance
        { get 
            {
                if ( instance == null ) instance = new BookCollection();
                return instance;
            }
        }

        private BookCollection()
        {
            Collection = new List<Book>();
        }

        public List<Book> Collection
        { get; set; }
    }
}
