﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraryHelper.Objects;
namespace LibraryHelper.Singleton
{
    public class UserCollection
    {
        private static UserCollection instance = null;

        public static UserCollection Instance
        { get 
            {
                if ( instance == null ) instance = new UserCollection();
                return instance;
            }
        }

        private UserCollection()
        {
            Collection = new List<User>();
        }

        public List<User> Collection
        { get; set; }
    }
}
