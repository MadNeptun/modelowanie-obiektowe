﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraryHelper.Objects;
namespace LibraryHelper.Singleton
{
    public class AuthorCollection
    {
        private static AuthorCollection instance = null;

        public static AuthorCollection Instance
        { get 
            {
                if ( instance == null ) instance = new AuthorCollection();
                return instance;
            }
        }

        private AuthorCollection()
        {
            Collection = new List<Author>();
        }

        public List<Author> Collection
        { get; set; }
    }
}
